# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'machine', type: :request do
  require 'json'

  before(:each) do
    @event = FactoryBot.create(:event)
  end

  describe 'User is not logged' do
    it 'index returns ok' do
      get '/api/v1/events'
      expect(response).to have_http_status(:ok)
    end

    it 'show returns ok' do
      get "/api/v1/events/#{@event.id}"
      expect(response).to have_http_status(:ok)
    end

    xit 'search returns ok' do
      get '/api/v1/events?page=1&key=fortaleza'
      expect(response).to have_http_status(:ok)
    end

    it 'create returns unauthorized' do
      post '/api/v1/events'
      expect(response).to have_http_status(:unauthorized)
    end

    it 'update returns unauthorized' do
      put "/api/v1/events/#{@event.id}"
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'user authentication' do
    before(:each) do
      @artist = FactoryBot.create(:artist)
      @current_user = FactoryBot.create(:user)
      login
    end

    it 'index returns authorized' do
      get '/api/v1/events', headers: headers
      expect(response).to have_http_status(:ok)
    end

    it 'show returns authorized' do
      get "/api/v1/events/#{@event.id}", headers: headers
      expect(response).to have_http_status(:ok)
    end

    xit 'create returns authorized' do
      post '/api/v1/events', headers: headers, params: { event: attributes_for(:event) }
      expect(response).to have_http_status(:ok)
    end

    it 'update returns authorized' do
      put "/api/v1/events/#{@event.id}", headers: headers, params: { event: attributes_for(:event) }
      expect(response).to have_http_status(:ok)
    end
  end
end
