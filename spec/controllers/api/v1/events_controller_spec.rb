# frozen_string_literal: true

require 'rails_helper'
require 'json'

RSpec.describe Api::V1::EventsController, type: :controller do
  before(:each) do
    DatabaseFake.populate
  end

  describe 'GET #index' do
    it 'returns ten events order' do
      get :index
      json = JSON.parse(response.body)
      events = Event.all.order(created_at: :desc).limit(10)
      expect(json.count).to eql(10)
      expect(Event.count).to eql(20)
      events.each_with_index do |event, idx|
        expect(json[idx]['name']).to eql(event.name)
        expect(json[idx]['id']).to eql(event.id)
        expect(json[idx]['location']).to eql(event.location)
      end
    end
  end

  describe 'GET #show' do
    it 'returns http success' do
      event = FactoryBot.create(:event)
      get :show, params: { id: event.id }
      res = JSON.parse(response.body)
      expect(Event.count).to eql(21)
      expect(res['name']).to eql(event.name)
      expect(res['id']).to eql(event.id)
      expect(res['location']).to eql(event.location)
    end
  end

  context 'Test ElasticSearch to searching for event' do
    before(:each) do
      @event = FactoryBot.create(:event)
    end

    describe 'GET #search' do
      it 'search for location' do
        @event.update(location: 'Terra do Sol em Fortaleza')
        get :index, params: { key: 'TeRra Do Sool na Grande FortaleZA' }
        res = JSON.parse(response.body)
        expect(res.count).to eql(10)
        expect(Event.count).to eql(21)
        expect(res[0]['id']).to eql(@event.id)
        expect(res[0]['location']).to eql(@event.location)
      end

      it 'search for name' do
        @event.update(name: 'Vinicius Freire de Oliveira')
        get :index, params: { key: 'Vinicius Freire de 0l1ve1ra' }
        res = JSON.parse(response.body)
        expect(res.count).to eql(10)
        expect(Event.count).to eql(21)
        expect(res[0]['name']).to eql(@event.name)
        expect(res[0]['id']).to eql(@event.id)
      end
    end
  end

  describe 'PUT #update' do
    xit 'returns http success' do
      sign_in FactoryBot.create(:user)
      event = FactoryBot.create(:event)
      updated_event = attributes_for(:event)
      put :update, params: { id: event.id, event: updated_event }
      require 'pry'; binding.pry
      expect(response).to have_http_status(:success)
    end
  end

  describe 'DELETE #destroy' do
    xit 'returns http success' do
      get :destroy
      expect(response).to have_http_status(:success)
    end
  end
end
