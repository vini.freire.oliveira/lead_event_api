# frozen_string_literal: true

FactoryBot.define do
  factory :event do
    name { FFaker::NameBR.unique.name }
    user { FactoryBot.create(:user) }
    artists { [FactoryBot.create(:artist)] }
    time { Time.now.to_s }
    tz { 'America/New_York' }
    location { FFaker::AddressBR.city }
    genres { %w[pop hip-hop] }
  end
end
