# frozen_string_literal: true

FactoryBot.define do
  factory :artist do
    name { FFaker::NameBR.unique.name }
    link { FFaker::NameBR.unique.first_name }
    description { FFaker::Lorem.sentence }
    age { 30 }
  end
end
