# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EventPolicy, type: :policy do
  before(:each) do
    @user = FactoryBot.create(:user)
  end

  let(:event) { FactoryBot.create(:event) }
  let(:user) { EventPolicy.new(@user, event) }

  permissions :index? do
    context 'User see all events' do
      it 'grants access' do
        expect(user).to permitted_to(:index)
      end
    end
  end

  permissions :show? do
    context 'User see one event' do
      it 'grants access' do
        expect(user).to permitted_to(:show)
      end
    end
  end

  permissions :create? do
    context 'User create an event' do
      it 'grants access' do
        expect(user).to permitted_to(:create)
      end
    end
  end

  permissions :update? do
    context 'User create an event' do
      it 'grants access' do
        expect(user).to permitted_to(:update)
      end
    end
  end

  permissions :destroy? do
    context 'User create an event' do
      it 'grants access' do
        expect(user).to permitted_to(:destroy)
      end
    end
  end
end
