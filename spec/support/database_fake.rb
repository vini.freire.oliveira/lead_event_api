# frozen_string_literal: true

module DatabaseFake
  def self.populate
    (1..20).each do |_i|
      FactoryBot.create(:event)
    end
  end
end
