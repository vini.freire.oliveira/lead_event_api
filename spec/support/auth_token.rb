# frozen_string_literal: true

module AuthToken
  def login
    post '/auth/sign_in', params: { email: @current_user.email, password: 'secret123' }
  end

  def headers
    {
      "client": response.header['client'],
      "uid": response.header['uid'],
      "access-token": response.header['access-token']
    }
  end
end
