# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Artist, type: :model do
  before(:each) do
    @artist = FactoryBot.create(:artist)
  end

  describe 'Validations' do
    context 'can not be blank' do
      it 'name' do
        @artist.name = nil
        expect(@artist).to_not be_valid
      end
    end

    context 'can not repeat' do
      it 'name' do
        other_artist = FactoryBot.create(:artist)
        other_artist.name = @artist.name
        expect(other_artist).to_not be_valid
      end
    end
  end
end
