# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Event, type: :model do
  before(:each) do
    @event = FactoryBot.create(:event)
  end

  describe 'Validations' do
    context 'can not be blank' do
      it 'name' do
        @event.name = nil
        expect(@event).to_not be_valid
      end
    end

    context 'can not repeat' do
      it 'name' do
        other_event = FactoryBot.create(:event)
        other_event.name = @event.name
        expect(other_event).to_not be_valid
      end
    end
  end
end
