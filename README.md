* ## Lead Event Api

  #####  About

  This api was developed as part of the hiring process for Tech Leader  position.
  
  ![Description](https://gitlab.com/vini.freire.oliveira/lead_event_api/-/blob/master/docs/ror.pdf)


  #####  System dependencies

  - Docker (<https://www.docker.com/>)
  - Docker-compose

  

  #####  Architecture

  ![architecture](https://gitlab.com/vini.freire.oliveira/lead_event_api/-/blob/master/docs/architecture.png)



  #####  To run Rspec test results (Results in Docs)


  1. To avoid any Elasticsearch error on linux. Open the terminal and execute

     ```
     sudo sysctl -w vm.max_map_count=262144
     ```
    
    
  2. Now, create database and generate migrations:

     ```
     docker-compose run --rm app bundle exec rails db:drop db:create db:migrate
     ```


  3. To run the controller spec

     ```
     docker-compose run --rm app bundle exec rspec spec/controller
     ```

  4. To run all spec files

     ```
     docker-compose run --rm app bundle exec rspec spec
     ```

  3. To run and save all spec files

     ```
     docker-compose run --rm app bundle exec rspec spec/ --format h > docs/rspec-result.html
     ```

  

  #####  Step by step to run the api in localhost using docker-compose

  1. Clone the project.

     ```
     git clone https://gitlab.com/vini.freire.oliveira/lead_event_api.git
     ```

  2. Build the containers and install all dependencies

     ```
     docker-compose run --rm app bundle install
     ```

  3. To avoid any Elasticsearch error on linux. Open the terminal and execute:

     ```
     sudo sysctl -w vm.max_map_count=262144
     ```

  4. Now, create database and generate migrations:

     ```ruby
     docker-compose run --rm app bundle exec rails db:drop db:create db:migrate db:seed
     ```

  5. Start the server with

     ```
     docker-compose up
     ```


  #####  Step by step to run the react app on port 3001 in localhost using docker

  1. Clone the project.

     ```
     git clone https://gitlab.com/vini.freire.oliveira/lead_event_front.git
     ```

  2. Open the project folder and execute

     ```
     npm install
     ```

  3. Build the containers and install all dependencies

     ```
     docker build -t sample:dev .
     ```

  4. start the react app

     ```
     docker run -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm sample:dev
     ```



  ###  Links

  #####  [My Git](https://gitlab.com/vini.freire.oliveira) 

  #####  [My LikedIn](https://www.linkedin.com/in/vinicius-freire-b53507107/)
