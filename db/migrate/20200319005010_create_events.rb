# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.references :user, foreign_key: true
      t.string :name, null: false
      t.integer :kind
      t.time :time
      t.string :tz, default: 'UTC'
      t.string :location
      t.string :genres

      t.timestamps
    end
  end
end
