# frozen_string_literal: true

class AddArtistsAndEvents < ActiveRecord::Migration[5.2]
  def self.up
    create_table :artists_events do |t|
      t.references :artist, :event
    end
  end

  def self.down
    drop_table :artists_events
  end
end
