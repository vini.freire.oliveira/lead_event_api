# frozen_string_literal: true

class CreateArtists < ActiveRecord::Migration[5.2]
  def change
    create_table :artists do |t|
      t.string   :name,        null: false
      t.string   :link,        null: false
      t.string   :description, null: false
      t.string   :image
      t.integer :age
      t.timestamps
    end
  end
end
