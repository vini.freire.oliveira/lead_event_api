# frozen_string_literal: true

location = ['Fortaleza', 'Rua Dr. Sp', 'Ceara - Forataleza', 'Belem', 'Flexeiras', 'Osasco']
sample_genres = %w[country pop]
tz = ['America/New_York', 'EST', 'UTC']

user = User.create(email: 't@t.com', password: '12345678', password_confirmation: '12345678', name: 'ttt', nickname: 't')
artist = Artist.create(name: 'Buu', link: 'wiki', description: 'jjj', age: 30)

(1..100).each do |i|
  genres = sample_genres.sample(rand(1..2))
  time = Time.find_zone(tz.sample).parse(Time.now.to_s)
  Event.create(user: user, name: "Evento-#{i}", time: time, location: location.sample, artists: [artist], genres: genres)
end
