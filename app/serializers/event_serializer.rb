# frozen_string_literal: true

class EventSerializer < ActiveModel::Serializer
  attributes :id, :name, :genres, :artists, :_time, :tz, :location, :owner

  def owner
    @object.user
  end

  def _time
    Time.find_zone(@object.tz).parse(@object.time.to_s)
  end
end
