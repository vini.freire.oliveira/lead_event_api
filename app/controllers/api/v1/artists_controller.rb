# frozen_string_literal: true

module Api::V1
  class ArtistsController < ApiController
    before_action :authorize_artist
    before_action :set_artist, only: %i[show update destroy]

    def index
      @artists = Artist.all
      render json: @artists
    end

    def show
      render json: @artist
    end

    def update
      if @artist.update(artist_params)
        render json: @artist
      else
        render json: @artist.errors, status: :unprocessable_entity
      end
    end

    def create
      @artist = Artist.new(artist_params)
      if @artist.save
        render json: @artist
      else
        render json: @artist.errors, status: :unprocessable_entity
      end
    end

    def destroy
      if @artist.destroy
        render json: :destroyed
      else
        render json: @artist.errors, status: :unprocessable_entity
      end
    end

    private

    def artist_params
      params.require(:artist).permit(:name, :link, :description, :image, :age)
    end

    def set_artist
      @artist = Artist.find(params[:id])
    end

    def authorize_artist
      authorize Artist
    end
  end
end
