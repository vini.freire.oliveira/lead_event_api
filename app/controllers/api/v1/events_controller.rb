# frozen_string_literal: true

module Api::V1
  class EventsController < ApiController
    before_action :authenticate_user!, except: %i[index show search]
    before_action :authorize_event
    before_action :set_event, only: %i[show update destroy]

    def index
      render json: get_events
    end

    def show
      render json: @event
    end

    def update
      if @event.update(event_params.merge(artists))
        render json: @event
      else
        render json: @event.errors, status: :unprocessable_entity
      end
    end

    def create
      @event = Event.new(event_params.merge(artists).merge({ user: current_user }))
      set_genres
      if @event.save
        render json: @event
      else
        render json: @event.errors, status: :unprocessable_entity
      end
    end

    def destroy
      if @event.destroy
        render json: :destroyed
      else
        render json: @event.errors, status: :unprocessable_entity
      end
    end

    private

    def event_params
      params.require(:event).permit(:name, :time, :location, :genres, :artists_ids, :tz)
    end

    def set_event
      @event = Event.find(params[:id])
    end

    def artists
      { artists: Artist.where(id: params[:artists_ids]) }
    end

    def set_genres
      @event.genres = params[:genres]
    end

    def authorize_event
      authorize Event
    end

    def get_events
      return Event.all.order(created_at: :desc).page(params[:page]) if params[:key].nil?

      elastic_events = Event.search(params[:key]).page(params[:page])
      
      # elastic_events = Autocompleter.call(params[:key])
      EventService.new(elastic_events.entries).perform
    end
  end
end
