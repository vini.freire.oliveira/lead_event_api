# frozen_string_literal: true

class Api::V1::ApiController < ApplicationController
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_action :authenticate_user!
  include Pundit

  private

  def user_not_authorized
    render json: { message: 'user not authorized' }.to_json, status: :unauthorized
  end
end
