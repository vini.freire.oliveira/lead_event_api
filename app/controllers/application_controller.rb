# frozen_string_literal: true

class ApplicationController < ActionController::API
  before_action :configure_permitted_parameters, if: :devise_controller?
  include DeviseTokenAuth::Concerns::SetUserByToken

  def index
    render json: { status: 'sucess' }
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[name nickname image])
    devise_parameter_sanitizer.permit(:account_update, keys: %i[name nickname image])
  end
end
