# frozen_string_literal: true

class Event < ApplicationRecord
  include Searchable

  belongs_to :user
  has_and_belongs_to_many :artists

  serialize :genres, Array

  validates :name, presence: true, uniqueness: true

  enum kind: %i[concert festival]

  before_validation :set_kind

  settings index: { number_of_shards: 1 } do
    mappings dynamic: 'false' do
      indexes :name
      indexes :location
      indexes :genres
      indexes :artists do
        indexes :name
        indexes :description
        indexes :link
      end
      indexes :kind
      indexes :time
    end
  end

  private

  def set_kind
    return self.kind = :concert if genres.count == 1

    self.kind = :festival
  end
end
