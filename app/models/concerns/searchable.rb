# frozen_string_literal: true

require 'elasticsearch/model'

module Searchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    # def self.search(query)
    #   __elasticsearch__.search(
    #     {
    #       query: {
    #         multi_match: {
    #           query: query,
    #           fields: ['name^10', 'location', 'artists.name', 'artists.description', 'genres']
    #         }
    #       }
    #     }
    #   )
    # end

    after_commit :index_document, if: :persisted?
    after_commit on: [:destroy] do
      __elasticsearch__.delete_document
    end
  end

  private

  def index_document
    __elasticsearch__.index_document
  end
end
