# frozen_string_literal: true

class EventResultBuilder < ResultBuilderBase
  def autocomplete_hint
    "#{record.name}, #{record.location}"
  end
end
