# frozen_string_literal: true

class EventService
  def initialize(events)
    @events = events
  end

  def perform
    filtered_event
  end

  private

  def filtered_event
    events = []
    @events.each do |event|
      events << format(event['_source'])
    end
    events
  end

  def format(event)
    {
      id: event['id'],
      name: event['name'],
      genres: event['genres'],
      artists: data(event['id']).artists,
      _time: Time.find_zone(event['tz']).parse(event['time']),
      tz: event['tz'],
      location: event['location'],
      owner: data(event['id']).user
    }
  end

  def data(id)
    @data ||= Event.find(id)
  end
end
